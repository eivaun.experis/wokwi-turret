# Wokwi - Turret

The project simulates a gun turret.

There's two servos controlling the azimuth and elevation.
When the fire button is pressed the led links and the buzzer plays a tone.
The current azimuth and elevation, and the remaining ammo is displayed on the LCD.

## Screenshot

![screenshot](screenshot.png)


## Contributors

Eivind Vold Aunebakk
